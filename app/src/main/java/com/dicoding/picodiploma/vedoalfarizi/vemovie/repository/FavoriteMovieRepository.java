package com.dicoding.picodiploma.vedoalfarizi.vemovie.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;

import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.AppDatabase;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.FavoriteMovieDao;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.FavoriteMovieTable;

import java.util.List;

public class FavoriteMovieRepository {
    private FavoriteMovieDao mFavoriteMovieDao;
    private LiveData<List<FavoriteMovieTable>> mAllFavoriteMovies;
    private MutableLiveData<Integer> favoriteResult = new MutableLiveData<>();

    public FavoriteMovieRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        mFavoriteMovieDao = database.favoriteMovieDao();
        mAllFavoriteMovies = mFavoriteMovieDao.getFavoriteMovies();
    }

    public LiveData<List<FavoriteMovieTable>> getAllFavoriteMovies(){
        return mAllFavoriteMovies;
    }

    public void isFavorite(int idMovie){
        checkAsyncTask task = new checkAsyncTask(mFavoriteMovieDao);
        task.favoriteMovieRepository = this;
        task.execute(idMovie);
    }

    public MutableLiveData<Integer> getFavoriteResult(){
        return favoriteResult;
    }

    public void insert(FavoriteMovieTable favoriteMovieTable){
        new insertAsyncTask(mFavoriteMovieDao).execute(favoriteMovieTable);
    }

    public void delete(int idMovie){
        deleteAsyncTask task = new deleteAsyncTask(mFavoriteMovieDao);
        task.execute(idMovie);
    }

    private static class checkAsyncTask extends AsyncTask<Integer, Void, Integer> {
        private FavoriteMovieDao mAsyncTaskFavoriteMovieDao;
        private FavoriteMovieRepository favoriteMovieRepository = null;

        checkAsyncTask(FavoriteMovieDao dao){
            mAsyncTaskFavoriteMovieDao = dao;
        }

        @Override
        protected Integer doInBackground(Integer... integers) {
            return mAsyncTaskFavoriteMovieDao.isFavorite(integers[0]);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            favoriteMovieRepository.favoriteResult.postValue(integer);
        }
    }

    private static class insertAsyncTask extends AsyncTask<FavoriteMovieTable, Void, Void>{
        private FavoriteMovieDao mAsyncTaskFavoriteMovieDao;

        insertAsyncTask(FavoriteMovieDao dao){
            mAsyncTaskFavoriteMovieDao = dao;
        }

        @Override
        protected Void doInBackground(FavoriteMovieTable... favoriteMovieTables) {
            mAsyncTaskFavoriteMovieDao.insertFavoriteMovie(favoriteMovieTables[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Integer, Void, Void> {
        private FavoriteMovieDao mAsyncTaskFavoriteMovieDao;

        deleteAsyncTask(FavoriteMovieDao dao){
            mAsyncTaskFavoriteMovieDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            mAsyncTaskFavoriteMovieDao.deleteOneFavoriteMovie(integers[0]);
            return null;
        }
    }

}
