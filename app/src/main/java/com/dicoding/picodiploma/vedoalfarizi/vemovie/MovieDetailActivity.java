package com.dicoding.picodiploma.vedoalfarizi.vemovie;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.FavoriteMovieTable;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.model.Movie;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.rest.ApiClient;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.viewmodel.FavoriteMovieViewModel;

public class MovieDetailActivity extends AppCompatActivity{
    FavoriteMovieViewModel favoriteMovieViewModel;
    Movie movie;
    int favoriteStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);

        favoriteMovieViewModel = ViewModelProviders.of(this).get(FavoriteMovieViewModel.class);

        ImageView imgPoster = findViewById(R.id.img_poster);
        TextView txtTitle = findViewById(R.id.tv_title);
        TextView txtRating = findViewById(R.id.tv_rating);
        TextView txtOverview = findViewById(R.id.tv_overview);
        TextView txtReleaseDate = findViewById(R.id.tv_release_date);
        RatingBar rbRating = findViewById(R.id.rb_rating);
        ProgressBar pbData = findViewById(R.id.pb_data);
        final ImageView btnFavorite = findViewById(R.id.btn_favorite);

        pbData.setVisibility(View.VISIBLE);

        Intent intent = getIntent();
        if (intent != null) {
            movie = intent.getParcelableExtra("movie_extra_key");
            txtTitle.setText(movie.getTitle());
            txtRating.setText(String.valueOf(movie.getRating()));
            txtOverview.setText(movie.getOverview());
            txtReleaseDate.setText(movie.getRelease_date());
            rbRating.setRating(movie.getRating() / 2);

            String poster_path = ApiClient.POSTER_URL +movie.getPoster();
            Glide.with(imgPoster).load(poster_path).into(imgPoster);

            favoriteMovieViewModel.isFavorite(movie.getId());
            favoriteMovieViewModel.getFavoriteResult().observe(this,
                    new Observer<Integer>() {
                @Override
                public void onChanged(@Nullable Integer integer) {
                    if(integer == 1){
                        btnFavorite.setImageResource(R.drawable.ic_favorite_pink_24dp);
                        favoriteStatus = 1;
                    }else{
                        btnFavorite.setImageResource(R.drawable.ic_favorite_black_empty_24dp);
                        favoriteStatus = 0;
                    }
                }
            });

            btnFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(favoriteMovieViewModel.getFavoriteResult().getValue() == 1){
                        favoriteMovieViewModel.delete(movie.getId());
                    }else{
                        FavoriteMovieTable movieTable = new FavoriteMovieTable();
                        movieTable.setId(movie.getId());
                        movieTable.setTitle(movie.getTitle());
                        movieTable.setOverview(movie.getOverview());
                        movieTable.setRelease_date(movie.getRelease_date());
                        movieTable.setRating(movie.getRating());
                        movieTable.setPoster(movie.getPoster());

                        favoriteMovieViewModel.insert(movieTable);
                    }
                    recreate();
                }
            });
        }

        pbData.setVisibility(View.INVISIBLE);
    }
}
