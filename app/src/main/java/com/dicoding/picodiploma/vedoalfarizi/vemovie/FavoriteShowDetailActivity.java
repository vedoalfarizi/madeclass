package com.dicoding.picodiploma.vedoalfarizi.vemovie;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.FavoriteShowTable;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.rest.ApiClient;

public class FavoriteShowDetailActivity extends AppCompatActivity {
    FavoriteShowTable showTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_show_detail);

        ImageView imgPoster = findViewById(R.id.img_poster);
        TextView txtTitle = findViewById(R.id.tv_title);
        TextView txtRating = findViewById(R.id.tv_rating);
        TextView txtOverview = findViewById(R.id.tv_overview);
        TextView txtPremiereDate = findViewById(R.id.tv_premiere_date);
        RatingBar rbRating = findViewById(R.id.rb_rating);

        Intent intent = getIntent();
        if(intent != null){
            showTable = intent.getParcelableExtra("favorite_show_extra_key");
            txtTitle.setText(showTable.getTitle());
            txtRating.setText(String.valueOf(showTable.getRating()));
            txtOverview.setText(showTable.getOverview());
            txtPremiereDate.setText(showTable.getPremiere());
            rbRating.setRating(showTable.getRating() / 2);

            String poster_path = ApiClient.POSTER_URL +showTable.getPhoto();
            Glide.with(imgPoster).load(poster_path).into(imgPoster);
        }
    }
}
