package com.dicoding.picodiploma.vedoalfarizi.vemovie.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;

import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.AppDatabase;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.FavoriteShowDao;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.FavoriteShowTable;

import java.util.List;

public class FavoriteShowRepository {
    private FavoriteShowDao mFavoriteShowDao;
    private LiveData<List<FavoriteShowTable>> mAllFavoriteShow;
    private MutableLiveData<Integer> favoriteResult = new MutableLiveData<>();

    public FavoriteShowRepository(Application application){
        AppDatabase database = AppDatabase.getInstance(application);
        mFavoriteShowDao = database.favoriteShowDao();
        mAllFavoriteShow = mFavoriteShowDao.getFavoriteShows();
    }

    public LiveData<List<FavoriteShowTable>> getAllFavoriteShow(){
        return mAllFavoriteShow;
    }

    public void isFavorite(int idShow){
        checkAsyncTask task = new checkAsyncTask(mFavoriteShowDao);
        task.favoriteShowRepository = this;
        task.execute(idShow);
    }

    public MutableLiveData<Integer> getFavoriteResult(){
        return favoriteResult;
    }

    public void insert(FavoriteShowTable favoriteShowTable){
        new insertAsyncTask(mFavoriteShowDao).execute(favoriteShowTable);
    }

    public void delete(int idShow){
        deleteAsyncTask task = new deleteAsyncTask(mFavoriteShowDao);
        task.execute(idShow);
    }

    private static class checkAsyncTask extends AsyncTask<Integer, Void, Integer>{
        private FavoriteShowDao mAsyncTaskFavoriteShowDao;
        private FavoriteShowRepository favoriteShowRepository = null;

        checkAsyncTask(FavoriteShowDao dao){
            mAsyncTaskFavoriteShowDao = dao;
        }

        @Override
        protected Integer doInBackground(Integer... integers) {
            return mAsyncTaskFavoriteShowDao.isFavorite(integers[0]);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            favoriteShowRepository.favoriteResult.postValue(integer);
        }
    }

    public static class insertAsyncTask extends AsyncTask<FavoriteShowTable, Void, Void>{
        private FavoriteShowDao mAsyncTaskFavoriteShowDao;

        insertAsyncTask(FavoriteShowDao dao){
            mAsyncTaskFavoriteShowDao = dao;
        }

        @Override
        protected Void doInBackground(FavoriteShowTable... favoriteShowTables) {
            mAsyncTaskFavoriteShowDao.insertFavoriteShow(favoriteShowTables[0]);
            return null;
        }
    }

    public static class deleteAsyncTask extends AsyncTask<Integer, Void, Void>{
        private FavoriteShowDao mAsyncTaskFavoriteShowDao;

        deleteAsyncTask(FavoriteShowDao dao){
            mAsyncTaskFavoriteShowDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            mAsyncTaskFavoriteShowDao.deleteOneFavoriteShow(integers[0]);
            return null;
        }
    }
}
