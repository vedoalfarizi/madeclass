package com.dicoding.picodiploma.vedoalfarizi.vemovie.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShowResults {
    @SerializedName("results")
    private List<Show> results;

    public List<Show> getResults() {
        return results;
    }
}
