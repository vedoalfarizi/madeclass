package com.dicoding.picodiploma.vedoalfarizi.vemovie.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity(tableName = "favorite_shows")
public class FavoriteShowTable implements Parcelable {
    @PrimaryKey
    private int id;

    @ColumnInfo(name = "name")
    private String title;

    @ColumnInfo(name = "overview")
    private String overview;

    @ColumnInfo(name = "first_air_date")
    private String premiere;

    @ColumnInfo(name = "vote_average")
    private float rating;

    @ColumnInfo(name = "poster_path")
    private String photo;

    public FavoriteShowTable() {
    }

    protected FavoriteShowTable(Parcel in) {
        id = in.readInt();
        title = in.readString();
        overview = in.readString();
        premiere = in.readString();
        rating = in.readFloat();
        photo = in.readString();
    }

    public static final Creator<FavoriteShowTable> CREATOR = new Creator<FavoriteShowTable>() {
        @Override
        public FavoriteShowTable createFromParcel(Parcel in) {
            return new FavoriteShowTable(in);
        }

        @Override
        public FavoriteShowTable[] newArray(int size) {
            return new FavoriteShowTable[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPremiere() {
        return premiere;
    }

    public void setPremiere(String premiere) {
        this.premiere = premiere;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(overview);
        dest.writeString(premiere);
        dest.writeFloat(rating);
        dest.writeString(photo);
    }
}
