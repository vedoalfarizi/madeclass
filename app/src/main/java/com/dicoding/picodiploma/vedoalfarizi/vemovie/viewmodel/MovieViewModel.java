package com.dicoding.picodiploma.vedoalfarizi.vemovie.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.util.Log;

import com.dicoding.picodiploma.vedoalfarizi.vemovie.BuildConfig;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.model.Movie;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.model.MovieResults;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.rest.ApiClient;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieViewModel extends ViewModel {
    private static final String API_KEY = BuildConfig.ApiKey;
    private MutableLiveData<ArrayList<Movie>> listMovieLive = new MutableLiveData<>();

    public MutableLiveData<ArrayList<Movie>> getListMovieLive() {
        return listMovieLive;
    }

    public void loadMovies() {
        String language = Locale.getDefault().toLanguageTag();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<MovieResults> call = apiService.getMovieResults(API_KEY, language);
        call.enqueue(new Callback<MovieResults>() {
            @Override
            public void onResponse(@NonNull Call<MovieResults> call, @NonNull Response<MovieResults> response) {
                List<Movie> movies;
                ArrayList<Movie> movieArrayList = new ArrayList<>();

                if(response.body() != null){
                    movies = response.body().getResults();
                    for(Movie data : movies){
                        Movie movie = new Movie();
                        movie.setId(data.getId());
                        movie.setTitle(data.getTitle());
                        movie.setOverview(data.getOverview());
                        movie.setRelease_date(data.getRelease_date());
                        movie.setRating(data.getRating());
                        movie.setPoster(data.getPoster());

                        movieArrayList.add(movie);
                    }

                    listMovieLive.postValue(movieArrayList);
                }
            }

            @Override
            public void onFailure(@NonNull Call<MovieResults> call, @NonNull Throwable t) {
                Log.d("onFailure", t.getMessage());
            }
        });
    }
}