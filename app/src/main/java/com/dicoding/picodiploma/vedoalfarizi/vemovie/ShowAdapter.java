package com.dicoding.picodiploma.vedoalfarizi.vemovie;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.model.Show;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.rest.ApiClient;

import java.util.ArrayList;

public class ShowAdapter extends RecyclerView.Adapter<ShowAdapter.ShowHolder> {
    private ArrayList<Show> listShows;
    private OnItemClick onItemClick;

    ShowAdapter() {
    }

    void setListShows(ArrayList<Show> listShows) {
        this.listShows = listShows;
        notifyDataSetChanged();
    }

    void setOnItemClick(OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public ShowAdapter.ShowHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_show, viewGroup, false);
        return new ShowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ShowAdapter.ShowHolder showHolder, int i) {
        Show show = listShows.get(i);

        String poster_path = ApiClient.POSTER_URL +show.getPhoto();

        Glide.with(showHolder.itemView.getContext())
                .load(poster_path)
                .into(showHolder.imgPoster);

        showHolder.tvTitle.setText(show.getTitle());
        showHolder.tvRating.setText(String.valueOf(show.getRating()));
        showHolder.tvPremiereDate.setText(show.getPremiere());
        showHolder.rbRating.setRating(show.getRating()/2);

        showHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.click(listShows.get(showHolder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listShows.size();
    }

    class ShowHolder extends RecyclerView.ViewHolder {
        ImageView imgPoster;
        TextView tvTitle, tvRating, tvPremiereDate;
        RatingBar rbRating;

        ShowHolder(@NonNull View itemView) {
            super(itemView);
            imgPoster = itemView.findViewById(R.id.img_poster);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvRating = itemView.findViewById(R.id.tv_rating);
            tvPremiereDate = itemView.findViewById(R.id.tv_premiere_date);
            rbRating = itemView.findViewById(R.id.rb_rating);
        }
    }

    public interface OnItemClick{
        void click(Show s);
    }
}
