package com.dicoding.picodiploma.vedoalfarizi.vemovie;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.FavoriteShowTable;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.model.Show;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.rest.ApiClient;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.viewmodel.FavoriteShowViewModel;

public class ShowDetailActivity extends AppCompatActivity {
    FavoriteShowViewModel favoriteShowViewModel;
    Show show;
    int favoriteStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_detail);

        favoriteShowViewModel = ViewModelProviders.of(this).get(FavoriteShowViewModel.class);

        ImageView imgPoster = findViewById(R.id.img_poster);
        TextView txtTitle = findViewById(R.id.tv_title);
        TextView txtRating = findViewById(R.id.tv_rating);
        TextView txtOverview = findViewById(R.id.tv_overview);
        TextView txtPremiereDate = findViewById(R.id.tv_premiere_date);
        RatingBar rbRating = findViewById(R.id.rb_rating);
        ProgressBar pbData = findViewById(R.id.pb_data);
        final ImageView btnFavorite = findViewById(R.id.btn_favorite);

        pbData.setVisibility(View.VISIBLE);

        Intent intent = getIntent();
        if(intent != null){
            show = intent.getParcelableExtra("show_extra_key");
            txtTitle.setText(show.getTitle());
            txtRating.setText(String.valueOf(show.getRating()));
            txtOverview.setText(show.getOverview());
            txtPremiereDate.setText(show.getPremiere());
            rbRating.setRating(show.getRating()/2);

            String poster_path = ApiClient.POSTER_URL +show.getPhoto();
            Glide.with(imgPoster).load(poster_path).into(imgPoster);

            favoriteShowViewModel.isFavorite(show.getId());
            favoriteShowViewModel.getFavoriteResult().observe(this, new Observer<Integer>() {
                @Override
                public void onChanged(@Nullable Integer integer) {
                    if(integer == 1){
                        btnFavorite.setImageResource(R.drawable.ic_favorite_pink_24dp);
                        favoriteStatus = 1;
                    }else{
                        btnFavorite.setImageResource(R.drawable.ic_favorite_black_empty_24dp);
                        favoriteStatus = 0;
                    }
                }
            });

            btnFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(favoriteShowViewModel.getFavoriteResult().getValue() == 1){
                        favoriteShowViewModel.delete(show.getId());
                    }else{
                        FavoriteShowTable showTable = new FavoriteShowTable();
                        showTable.setId(show.getId());
                        showTable.setTitle(show.getTitle());
                        showTable.setOverview(show.getOverview());
                        showTable.setPremiere(show.getPremiere());
                        showTable.setRating(show.getRating());
                        showTable.setPhoto(show.getPhoto());

                        favoriteShowViewModel.insert(showTable);
                    }
                    recreate();
                }
            });
        }

        pbData.setVisibility(View.INVISIBLE);
    }
}
