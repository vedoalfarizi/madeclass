package com.dicoding.picodiploma.vedoalfarizi.vemovie.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface FavoriteMovieDao {
    @Query("SELECT * FROM favorite_movies")
    LiveData<List<FavoriteMovieTable>> getFavoriteMovies();

    @Query("SELECT COUNT(*) FROM favorite_movies WHERE id = :idMovie")
    int isFavorite(int idMovie);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFavoriteMovie(FavoriteMovieTable favoriteMovieTable);

    @Query("DELETE FROM favorite_movies WHERE id = :idMovie")
    void deleteOneFavoriteMovie(int idMovie);
}
