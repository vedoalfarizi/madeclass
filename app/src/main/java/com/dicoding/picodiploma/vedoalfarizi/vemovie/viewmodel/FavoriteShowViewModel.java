package com.dicoding.picodiploma.vedoalfarizi.vemovie.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.FavoriteShowTable;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.repository.FavoriteShowRepository;

import java.util.List;

public class FavoriteShowViewModel extends AndroidViewModel {
    private FavoriteShowRepository favoriteShowRepository;
    private LiveData<List<FavoriteShowTable>> allFavoriteShows;
    private MutableLiveData<Integer> favoriteResult;

    public FavoriteShowViewModel(@NonNull Application application) {
        super(application);
        favoriteShowRepository = new FavoriteShowRepository(application);
        allFavoriteShows = favoriteShowRepository.getAllFavoriteShow();
        favoriteResult = favoriteShowRepository.getFavoriteResult();
    }

    public LiveData<List<FavoriteShowTable>> getAllFavoriteShows(){
        return allFavoriteShows;
    }

    public void isFavorite(int idShow){
        favoriteShowRepository.isFavorite(idShow);
    }

    public MutableLiveData<Integer> getFavoriteResult(){
        return favoriteResult;
    }

    public void insert(FavoriteShowTable favoriteShowTable){
        favoriteShowRepository.insert(favoriteShowTable);
    }

    public void delete(int idShow){
        favoriteShowRepository.delete(idShow);
    }
}
