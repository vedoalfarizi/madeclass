package com.dicoding.picodiploma.vedoalfarizi.vemovie.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface FavoriteShowDao {
    @Query("SELECT * FROM favorite_shows")
    LiveData<List<FavoriteShowTable>> getFavoriteShows();

    @Query("SELECT COUNT(*) FROM favorite_shows WHERE id = :idShow")
    int isFavorite(int idShow);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFavoriteShow(FavoriteShowTable favoriteShowTable);

    @Query("DELETE FROM favorite_shows WHERE id = :idShow")
    void deleteOneFavoriteShow(int idShow);
}
