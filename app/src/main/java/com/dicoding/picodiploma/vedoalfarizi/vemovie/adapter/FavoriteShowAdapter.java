package com.dicoding.picodiploma.vedoalfarizi.vemovie.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.R;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.FavoriteShowTable;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.rest.ApiClient;

import java.util.List;

public class FavoriteShowAdapter extends RecyclerView.Adapter<FavoriteShowAdapter.FavoriteShowMovieHolder> {
    private final LayoutInflater mInflater;
    private List<FavoriteShowTable> showTables;
    private OnItemClick onItemClick;

    public FavoriteShowAdapter(Context context){
        this.mInflater = LayoutInflater.from(context);
    }

    public void setShowTables(List<FavoriteShowTable> shows){
        showTables = shows;
        notifyDataSetChanged();
    }

    public int getShowTableId(int position){
        return showTables.get(position).getId();
    }

    public void setOnItemClick(OnItemClick onItemClick){
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public FavoriteShowMovieHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.item_favorite_show, viewGroup, false);
        return new FavoriteShowMovieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FavoriteShowMovieHolder favoriteShowMovieHolder, int i) {
        if(showTables != null){
            FavoriteShowTable current = showTables.get(i);

            String poster_path = ApiClient.POSTER_URL +current.getPhoto();
            Glide.with(favoriteShowMovieHolder.itemView.getContext())
                    .load(poster_path)
                    .into(favoriteShowMovieHolder.imgPoster);

            favoriteShowMovieHolder.tvTitle.setText(current.getTitle());
            favoriteShowMovieHolder.tvRating.setText(String.valueOf(current.getRating()));
            favoriteShowMovieHolder.tvPremiereDate.setText(current.getPremiere());
            favoriteShowMovieHolder.rbRating.setRating(current.getRating() / 2);

            favoriteShowMovieHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = favoriteShowMovieHolder.getAdapterPosition();
                    if(onItemClick != null && position != RecyclerView.NO_POSITION){
                        onItemClick.click(showTables.get(position));
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if(showTables != null){
            return showTables.size();
        }else{
            return 0;
        }
    }

    class FavoriteShowMovieHolder extends RecyclerView.ViewHolder {
        ImageView imgPoster;
        TextView tvTitle, tvRating, tvPremiereDate;
        RatingBar rbRating;

        FavoriteShowMovieHolder(@NonNull View itemView) {
            super(itemView);
            imgPoster = itemView.findViewById(R.id.img_poster);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvRating = itemView.findViewById(R.id.tv_rating);
            tvPremiereDate = itemView.findViewById(R.id.tv_premiere_date);
            rbRating = itemView.findViewById(R.id.rb_rating);
        }
    }

    public interface OnItemClick{
        void click(FavoriteShowTable showTable);
    }
}
