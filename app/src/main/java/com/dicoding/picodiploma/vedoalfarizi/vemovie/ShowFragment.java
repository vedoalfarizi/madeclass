package com.dicoding.picodiploma.vedoalfarizi.vemovie;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dicoding.picodiploma.vedoalfarizi.vemovie.model.Show;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.viewmodel.ShowViewModel;

import java.util.ArrayList;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class ShowFragment extends Fragment {
    private ShowAdapter showAdapter;
    private ProgressBar pbData;
    private ArrayList<Show> showArrayList = new ArrayList<>();

    public ShowFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) Objects.requireNonNull(getActivity())).setActionBarTitle("TV Shows");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_show, container, false);

        pbData = rootView.findViewById(R.id.pb_data);
        RecyclerView rvShow = rootView.findViewById(R.id.rv_show);
        rvShow.setLayoutManager(new LinearLayoutManager(getContext()));

        showAdapter = new ShowAdapter();
        showAdapter.setListShows(showArrayList);
        showAdapter.setOnItemClick(new ShowAdapter.OnItemClick() {
            @Override
            public void click(Show s) {
                Intent detailActivityIntent = new Intent(getActivity(), ShowDetailActivity.class);
                detailActivityIntent.putExtra("show_extra_key", s);
                startActivity(detailActivityIntent);
            }
        });
        rvShow.setAdapter(showAdapter);

        ShowViewModel showViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(ShowViewModel.class);
        pbData.setVisibility(View.VISIBLE);
        showViewModel.loadShows();
        showViewModel.getListShowLive().observe(this, getShow);

        return rootView;
    }

    private Observer<ArrayList<Show>> getShow = new Observer<ArrayList<Show>>() {
        @Override
        public void onChanged(@Nullable ArrayList<Show> shows) {
            if(shows != null){
                showAdapter.setListShows(shows);
            }else{
                Toast.makeText(getActivity(), "Data Not Found", Toast.LENGTH_SHORT).show();
            }
            pbData.setVisibility(View.INVISIBLE);
        }
    };
}
