package com.dicoding.picodiploma.vedoalfarizi.vemovie.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.R;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.FavoriteMovieTable;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.rest.ApiClient;

import java.util.List;

public class FavoriteMovieAdapter extends RecyclerView.Adapter<FavoriteMovieAdapter.FavoriteMovieViewHolder> {
    private final LayoutInflater mInflater;
    private List<FavoriteMovieTable> movieTables;
    private OnItemClick onItemClick;

    public FavoriteMovieAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
    }

    public void setMovieTables(List<FavoriteMovieTable> movies){
        movieTables = movies;
        notifyDataSetChanged();
    }

    public int getMovieTableId(int position){
        return movieTables.get(position).getId();
    }

    public void setOnItemClick(OnItemClick onItemClick){
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public FavoriteMovieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.item_favorite_movie, viewGroup, false);
        return new FavoriteMovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FavoriteMovieViewHolder favoriteMovieViewHolder, int i) {
        if(movieTables != null){
            FavoriteMovieTable current = movieTables.get(i);

            String poster_path = ApiClient.POSTER_URL+current.getPoster();
            Glide.with(favoriteMovieViewHolder.itemView.getContext())
                    .load(poster_path)
                    .into(favoriteMovieViewHolder.imgPoster);

            favoriteMovieViewHolder.tvTitle.setText(current.getTitle());
            favoriteMovieViewHolder.tvRating.setText(String.valueOf(current.getRating()));
            favoriteMovieViewHolder.tvReleaseDate.setText(current.getRelease_date());
            favoriteMovieViewHolder.rbRating.setRating(current.getRating()/2);

            favoriteMovieViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = favoriteMovieViewHolder.getAdapterPosition();
                    if(onItemClick != null && position != RecyclerView.NO_POSITION){
                        onItemClick.click(movieTables.get(position));
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if(movieTables != null){
            return movieTables.size();
        }else{
            return 0;
        }
    }

    class FavoriteMovieViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPoster;
        TextView tvTitle, tvRating, tvReleaseDate;
        RatingBar rbRating;

        FavoriteMovieViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPoster = itemView.findViewById(R.id.img_poster);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvRating = itemView.findViewById(R.id.tv_rating);
            tvReleaseDate = itemView.findViewById(R.id.tv_release_date);
            rbRating = itemView.findViewById(R.id.rb_rating);
        }
    }

    public interface OnItemClick{
        void click(FavoriteMovieTable movieTable);
    }
}
