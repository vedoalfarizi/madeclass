package com.dicoding.picodiploma.vedoalfarizi.vemovie.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.FavoriteMovieTable;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.repository.FavoriteMovieRepository;

import java.util.List;

public class FavoriteMovieViewModel extends AndroidViewModel {
    private FavoriteMovieRepository favoriteMovieRepository;
    private LiveData<List<FavoriteMovieTable>> allFavoriteMovies;
    private MutableLiveData<Integer> favoriteResult;

    public FavoriteMovieViewModel(@NonNull Application application) {
        super(application);
        favoriteMovieRepository = new FavoriteMovieRepository(application);
        allFavoriteMovies = favoriteMovieRepository.getAllFavoriteMovies();
        favoriteResult = favoriteMovieRepository.getFavoriteResult();

    }

    public LiveData<List<FavoriteMovieTable>> getAllFavoriteMovies(){
        return allFavoriteMovies;
    }

    public void isFavorite(int idMovie){
        favoriteMovieRepository.isFavorite(idMovie);
    }

    public MutableLiveData<Integer> getFavoriteResult(){
        return favoriteResult;
    }

    public void insert(FavoriteMovieTable favoriteMovieTable){
        favoriteMovieRepository.insert(favoriteMovieTable);
    }

    public void delete(int idMovie){
        favoriteMovieRepository.delete(idMovie);
    }
}
