package com.dicoding.picodiploma.vedoalfarizi.vemovie;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dicoding.picodiploma.vedoalfarizi.vemovie.adapter.FavoriteMovieAdapter;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.FavoriteMovieTable;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.viewmodel.FavoriteMovieViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteMovieFragment extends Fragment {
    private FavoriteMovieAdapter adapter;
    private List<FavoriteMovieTable> movieTables = new ArrayList<>();

    public FavoriteMovieFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_favorite_movie, container, false);
        RecyclerView rvMovie = rootView.findViewById(R.id.rv_favorite_movie);
        rvMovie.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter = new FavoriteMovieAdapter(getContext());
        adapter.setMovieTables(movieTables);
        rvMovie.setAdapter(adapter);

        final FavoriteMovieViewModel viewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(FavoriteMovieViewModel.class);
        viewModel.getAllFavoriteMovies().observe(this, getFavoriteMovie);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                viewModel.delete(adapter.getMovieTableId(viewHolder.getAdapterPosition()));
                Toast.makeText(getActivity(), "Dihapus dari Favorite", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(rvMovie);

        adapter.setOnItemClick(new FavoriteMovieAdapter.OnItemClick() {
            @Override
            public void click(FavoriteMovieTable movieTable) {
                Intent detailActivityIntent = new Intent(getActivity(), FavoriteMovieDetailActivity.class);
                detailActivityIntent.putExtra("favorite_movie_extra_key", movieTable);
                startActivity(detailActivityIntent);
            }
        });

        return rootView;
    }

    private Observer<List<FavoriteMovieTable>> getFavoriteMovie = new Observer<List<FavoriteMovieTable>>() {
        @Override
        public void onChanged(@Nullable List<FavoriteMovieTable> favoriteMovieTables) {
            if(favoriteMovieTables != null){
                adapter.setMovieTables(favoriteMovieTables);
            }else{
                Toast.makeText(getActivity(), "Data tidak ada", Toast.LENGTH_SHORT).show();
            }
        }
    };

}
