package com.dicoding.picodiploma.vedoalfarizi.vemovie.rest;

import com.dicoding.picodiploma.vedoalfarizi.vemovie.model.MovieResults;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.model.ShowResults;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("/3/discover/movie")
    Call<MovieResults> getMovieResults(
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    @GET("/3/discover/tv")
    Call<ShowResults> getShowResults(
            @Query("api_key") String apiKey,
            @Query("language") String language
    );
}
