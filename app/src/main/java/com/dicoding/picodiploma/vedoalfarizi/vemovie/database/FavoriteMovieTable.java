package com.dicoding.picodiploma.vedoalfarizi.vemovie.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity(tableName = "favorite_movies")
public class FavoriteMovieTable implements Parcelable {
    @PrimaryKey
    private int id;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "overview")
    private String overview;

    @ColumnInfo(name = "release_date")
    private String release_date;

    @ColumnInfo(name = "vote_average")
    private float rating;

    @ColumnInfo(name = "poster_path")
    private String poster;

    public FavoriteMovieTable() {

    }

    protected FavoriteMovieTable(Parcel in) {
        id = in.readInt();
        title = in.readString();
        overview = in.readString();
        release_date = in.readString();
        rating = in.readFloat();
        poster = in.readString();
    }

    public static final Creator<FavoriteMovieTable> CREATOR = new Creator<FavoriteMovieTable>() {
        @Override
        public FavoriteMovieTable createFromParcel(Parcel in) {
            return new FavoriteMovieTable(in);
        }

        @Override
        public FavoriteMovieTable[] newArray(int size) {
            return new FavoriteMovieTable[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(overview);
        dest.writeString(release_date);
        dest.writeFloat(rating);
        dest.writeString(poster);
    }
}
