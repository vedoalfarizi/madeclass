package com.dicoding.picodiploma.vedoalfarizi.vemovie;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dicoding.picodiploma.vedoalfarizi.vemovie.model.Movie;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.viewmodel.MovieViewModel;

import java.util.ArrayList;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieFragment extends Fragment {
    private MovieAdapter movieAdapter;
    private ProgressBar pbData;
    private ArrayList<Movie> movieArrayList = new ArrayList<>();

    public MovieFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) Objects.requireNonNull(getActivity())).setActionBarTitle("Movies");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_movie, container, false);

        pbData = rootView.findViewById(R.id.pb_data);
        RecyclerView rvMovie = rootView.findViewById(R.id.rv_movie);
        rvMovie.setLayoutManager(new LinearLayoutManager(getContext()));

        movieAdapter = new MovieAdapter();
        movieAdapter.setListMovies(movieArrayList);
        movieAdapter.setOnItemClick(new MovieAdapter.OnItemClick() {
            @Override
            public void click(Movie m) {
                Intent detailActivityIntent = new Intent(getActivity(), MovieDetailActivity.class);
                detailActivityIntent.putExtra("movie_extra_key", m);
                startActivity(detailActivityIntent);
            }
        });
        rvMovie.setAdapter(movieAdapter);

        MovieViewModel movieViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(MovieViewModel.class);
        pbData.setVisibility(View.VISIBLE);
        movieViewModel.loadMovies();
        movieViewModel.getListMovieLive().observe(this, getMovie);

        return rootView;
    }

    private Observer<ArrayList<Movie>> getMovie = new Observer<ArrayList<Movie>>() {
        @Override
        public void onChanged(@Nullable ArrayList<Movie> movies) {
            if(movies != null){
                movieAdapter.setListMovies(movies);
            }else{
                Toast.makeText(getActivity(), "Data not found", Toast.LENGTH_SHORT).show();
            }
            pbData.setVisibility(View.INVISIBLE);
        }
    };
}
