package com.dicoding.picodiploma.vedoalfarizi.vemovie.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieResults {
    @SerializedName("results")
    private
    List<Movie> results;

    public List<Movie> getResults() {
        return results;
    }

    public void setResults(List<Movie> results) {
        this.results = results;
    }
}
