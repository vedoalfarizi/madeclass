package com.dicoding.picodiploma.vedoalfarizi.vemovie;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.FavoriteMovieTable;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.rest.ApiClient;

public class FavoriteMovieDetailActivity extends AppCompatActivity {
    FavoriteMovieTable movieTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_movie_detail);

        ImageView imgPoster = findViewById(R.id.img_poster);
        TextView txtTitle = findViewById(R.id.tv_title);
        TextView txtRating = findViewById(R.id.tv_rating);
        TextView txtOverview = findViewById(R.id.tv_overview);
        TextView txtReleaseDate = findViewById(R.id.tv_release_date);
        RatingBar rbRating = findViewById(R.id.rb_rating);

        Intent intent = getIntent();
        if(intent != null){
            movieTable = intent.getParcelableExtra("favorite_movie_extra_key");
            txtTitle.setText(movieTable.getTitle());
            txtRating.setText(String.valueOf(movieTable.getRating()));
            txtOverview.setText(movieTable.getOverview());
            txtReleaseDate.setText(movieTable.getRelease_date());
            rbRating.setRating(movieTable.getRating()/2);

            String poster_path = ApiClient.POSTER_URL +movieTable.getPoster();
            Glide.with(imgPoster).load(poster_path).into(imgPoster);
        }
    }
}
