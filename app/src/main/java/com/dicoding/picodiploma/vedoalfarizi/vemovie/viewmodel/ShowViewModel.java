package com.dicoding.picodiploma.vedoalfarizi.vemovie.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.util.Log;

import com.dicoding.picodiploma.vedoalfarizi.vemovie.BuildConfig;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.model.Show;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.model.ShowResults;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.rest.ApiClient;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowViewModel extends ViewModel {
    private static final String API_KEY = BuildConfig.ApiKey;
    private MutableLiveData<ArrayList<Show>> listShowLive = new MutableLiveData<>();

    public MutableLiveData<ArrayList<Show>> getListShowLive() {
        return listShowLive;
    }

    public void loadShows(){
        String language = Locale.getDefault().toLanguageTag();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ShowResults> call = apiService.getShowResults(API_KEY, language);
        call.enqueue(new Callback<ShowResults>() {
            @Override
            public void onResponse(@NonNull Call<ShowResults> call, @NonNull Response<ShowResults> response) {
                List<Show> shows;
                ArrayList<Show> showArrayList = new ArrayList<>();

                if(response.body() != null){
                    shows = response.body().getResults();
                    for(Show data : shows){
                        Show show = new Show();
                        show.setId(data.getId());
                        show.setTitle(data.getTitle());
                        show.setOverview(data.getOverview());
                        show.setPremiere(data.getPremiere());
                        show.setRating(data.getRating());
                        show.setPhoto(data.getPhoto());

                        showArrayList.add(show);
                    }

                    listShowLive.postValue(showArrayList);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ShowResults> call, @NonNull Throwable t) {
                Log.d("onFailure", t.getMessage());
            }
        });
    }
}