package com.dicoding.picodiploma.vedoalfarizi.vemovie;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dicoding.picodiploma.vedoalfarizi.vemovie.adapter.FavoriteShowAdapter;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.database.FavoriteShowTable;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.viewmodel.FavoriteShowViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteShowFragment extends Fragment {
    private FavoriteShowAdapter adapter;
    private List<FavoriteShowTable> showTables = new ArrayList<>();

    public FavoriteShowFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_favorite_show, container, false);
        RecyclerView rvShow = rootView.findViewById(R.id.rv_favorite_show);
        rvShow.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter = new FavoriteShowAdapter(getContext());
        adapter.setShowTables(showTables);
        rvShow.setAdapter(adapter);

        final FavoriteShowViewModel viewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(FavoriteShowViewModel.class);
        viewModel.getAllFavoriteShows().observe(this, getFavoriteShow);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                viewModel.delete(adapter.getShowTableId(viewHolder.getAdapterPosition()));
                Toast.makeText(getActivity(), "Dihapus dari Favorite", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(rvShow);

        adapter.setOnItemClick(new FavoriteShowAdapter.OnItemClick() {
            @Override
            public void click(FavoriteShowTable showTable) {
                Intent detailActivityIntent = new Intent(getActivity(), FavoriteShowDetailActivity.class);
                detailActivityIntent.putExtra("favorite_show_extra_key", showTable);
                startActivity(detailActivityIntent);
            }
        });

        return rootView;
    }

    private Observer<List<FavoriteShowTable>> getFavoriteShow = new Observer<List<FavoriteShowTable>>() {
        @Override
        public void onChanged(@Nullable List<FavoriteShowTable> favoriteShowTables) {
            if(favoriteShowTables != null){
                adapter.setShowTables(favoriteShowTables);
            }else{
                Toast.makeText(getActivity(), "Data tidak ada", Toast.LENGTH_SHORT).show();
            }
        }
    };

}
