package com.dicoding.picodiploma.vedoalfarizi.vemovie;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.model.Movie;
import com.dicoding.picodiploma.vedoalfarizi.vemovie.rest.ApiClient;

import java.util.ArrayList;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieHolder> {
    private ArrayList<Movie> listMovies;
    private OnItemClick onItemClick;

    MovieAdapter() {
    }

    void setListMovies(ArrayList<Movie> listMovies) {
        this.listMovies = listMovies;
        notifyDataSetChanged();
    }

    void setOnItemClick(OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_movie, viewGroup, false);
        return new MovieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MovieHolder movieHolder, int i) {
        Movie movie = listMovies.get(i);

        String poster_path = ApiClient.POSTER_URL +movie.getPoster();

        Glide.with(movieHolder.itemView.getContext())
                .load(poster_path)
                .into(movieHolder.imgPoster);

        movieHolder.tvTitle.setText(movie.getTitle());
        movieHolder.tvRating.setText(String.valueOf(movie.getRating()));
        movieHolder.tvReleaseDate.setText(movie.getRelease_date());
        movieHolder.rbRating.setRating(movie.getRating() / 2);

        movieHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.click(listMovies.get(movieHolder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMovies.size();
    }

    class MovieHolder extends RecyclerView.ViewHolder {
        ImageView imgPoster;
        TextView tvTitle, tvRating, tvReleaseDate;
        RatingBar rbRating;

        MovieHolder(@NonNull View itemView) {
            super(itemView);
            imgPoster = itemView.findViewById(R.id.img_poster);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvRating = itemView.findViewById(R.id.tv_rating);
            tvReleaseDate = itemView.findViewById(R.id.tv_release_date);
            rbRating = itemView.findViewById(R.id.rb_rating);
        }
    }

    public interface OnItemClick {
        void click(Movie m);
    }
}
